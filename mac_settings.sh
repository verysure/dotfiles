#!/bin/bash

# dock
defaults write com.apple.dock "autohide" -bool "true"
defaults write com.apple.dock autohide-delay -float 0
defaults write com.apple.dock autohide-time-modifier -float 0
defaults write com.apple.dock "show-recents" -bool "false"
defaults write com.apple.dock "mineffect" -string "scale"
defaults write com.apple.dock "tilesize" -int "60"
defaults write com.apple.dock "wvous-tl-corner" -int 0
defaults write com.apple.dock "wvous-tr-corner" -int 0
defaults write com.apple.dock "wvous-bl-corner" -int 0
defaults write com.apple.dock "wvous-br-corner" -int 0
killall Dock

# keyboard
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false
defaults write com.apple.HIToolbox AppleFnUsageType -int "0"
defaults write -g InitialKeyRepeat -int 10
defaults write -g KeyRepeat -int 1
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false
defaults write NSGlobalDomain KB_DoubleQuoteOption -string "\"abc\""
defaults write NSGlobalDomain KB_SingleQuoteOption -string "'abc'"

# finder
defaults write NSGlobalDomain "AppleShowAllExtensions" -bool "true"
defaults write com.apple.finder "ShowPathbar" -bool "true"
defaults write com.apple.finder "FXPreferredViewStyle" -string "Nlsv"
defaults write com.apple.finder "_FXSortFoldersFirst" -bool "true"
defaults write com.apple.finder "FXEnableExtensionChangeWarning" -bool "false"
killall Finder

# trackpad
defaults write com.apple.AppleMultitouchTrackpad "FirstClickThreshold" -int "0"
defaults write com.apple.AppleMultitouchTrackpad "DragLock" -bool "false"
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 2

# desktop
defaults write com.apple.WindowManager EnableStandardClickToShowDesktop -bool "false"
