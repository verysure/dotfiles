verysure dotfiles setup
=======================


## Install and Init

- download repo: 
    - for tracking `git clone git@gitlab.com:verysure/dotfiles.git ~/.dotfiles`
    - for other machines only `git clone https://gitlab.com/verysure/dotfiles.git ~/.dotfiles`
- `./mac_init.sh` to install brew and OMZ for macos
- `./install` for linking dotfiles

## Usage

- edit any files as is
- backup current system changes:
    - `./backup` system changes
    - `git push` to make changes to repo
- update from repo: 
    - `git pull` to fetch the repo changes
    - `./install` to update changes to system





